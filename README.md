# HBee-Aria

HBee combined with AriaTech doser.


Clone repository:

```
git clone --recurse-submodules git@gitlab.com:agriconnect/embedded/hbee-aria.git
```

Need to clone with `submodules` because this project uses a library from https://gitlab.com/agriconnect/embedded/doser-arm-lib.
