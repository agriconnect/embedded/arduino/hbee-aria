/*
 * This is a sample configuration file for the "mqtt_esp8266_sensors" sensor.
 *
 * Change the settings below and save the file as "config.h"
 * You can then upload the code using the Arduino IDE.
 */

/*
 *  General.
 *  When preparing this node for a customer, please change these names.
 */
// Codename of the farm, where we deploy this node to.
// Can be passed from command line.
#ifndef FARM
  #define FARM "demo"
#endif

// Serial number. Must be lower case.
#ifndef SERIAL_NUMBER
  #define SERIAL_NUMBER "hb000000"
#endif

// WiFi
#define WIFI_CONNECT_TIMEOUT 20
#define MAX_LENGTH_WIFI_STRING 20
#define WIFI_SSID_1 "AgriConnectFarm"
#define WIFI_PASS_1 "trainamtrainam"

#define WIFI_SSID_2 "AgriConnect"
#define WIFI_PASS_2 "namthomngon"

#define WIFI_SSID_3 "Xom nha la"
#define WIFI_PASS_3 "AgriConnect"

/** MQTT
 * Global broker: mqtt.agriconnect.vn
 * Local (installed in farm): 192.168.8.10
 */
#define MQTT_HOST "mqtt.agriconnect.vn"           // MQTT broker
#define MQTT_USER "node"                      // User - connect to MQTT broker
#define MQTT_PASS "654321"                       // Password - connect to MQTT broker
#define MQTT_CLIENT_ID  SERIAL_NUMBER      // Must be unique on the MQTT network
#define MQTT_WAIT_RECONNECT 5
#define MQTT_MAX_EXPECTED_PAYLOAD_LENGTH 255

/** MQTT Topics
 **/
// MQTT topic to publish the environment condition. Example: myfarm/sn/AB000001
#define MQTT_TOPIC_NODE  FARM "/sn/" SERIAL_NUMBER
// MQTT topic to publish actuator status. The payload is JSON string, like {"1": 0, "2": 1}
#define MQTT_TOPIC_ACTUATOR_STATUS FARM "/sn/" SERIAL_NUMBER "/as/"
// MQTT topic to subscribe and get command to switch on/off actuator
#define MQTT_TOPIC_ACTUATOR_CONTROL FARM "/snac/" SERIAL_NUMBER "/"

// ESP32 GPIO to be used as UART1
#define UART1_RX   5
#define UART1_TX   4

// ARM communication
#define ARM_BAUDRATE 115200
#define MAX_CMD_LEN  256
#define CMD_SET_STATE  "SA %lu %d"
#define CMD_GET_ALL_ACT  "AA"
#define CMD_GET_ALL_ENV  "AE"

// Millis
#define INTERVAL_DEVICE_CHECK 3000                 // 3s
#define INTERVAL_ENVIRONMENT_CHECK 5000            // 5s

/** Color's LED
 *  Normal:                GREEN -> WHILE -> ...(flash)
 *  ARM response:          WHILE
 *  Reconnect wifi:        RED
 *  Reconnect MQTT-Broker: BLUE
**/
// Pin's LED
#define LED_WIFI_STATUS 25                        // LED red
#define LED_NORMAL_STATUS 26                      // LED green
#define LED_MQTT_STATUS 27                        // LED blue

// Data_EC in ARM response is uS/cm --> SENSOR_EC_PPM 0
#define SENSOR_EC_PPM 1
