#include <Arduino.h>
// WiFi
#include <WiFi.h>
// MQTT
#include <PubSubClient.h>
// JSON builder
#include <ArduinoJson.h>
// JSON parser. We use jsmn instead of ArduinoJson above for parsing JSON, to save memory. The input string may be long and the parsing result may be big.
#include <jsmn.h>
#include "doser-arm-lib.h"
/* Use ESP-IDF macro */
#ifdef ARDUINO_ARCH_ESP32
#include "esp32-hal-log.h"
#endif
// Set configuration options for WiFi, MQTT in the following file:
#include "config.h"

WiFiClient espClient;
unsigned long lastDeviceStatusCheck = 0;
unsigned long lastEnvironmentCheck = 0;

const char wifiLogins[][MAX_LENGTH_WIFI_STRING] = {
    WIFI_SSID_1, WIFI_PASS_1,
    WIFI_SSID_2, WIFI_PASS_2,
    WIFI_SSID_3, WIFI_PASS_3,
};

PubSubClient client(espClient);
// Use UART1 to communicate with ARM component (read sensors).
HardwareSerial arm_serial(1);

jsmn_parser jsParser;

/* Declaration of functions */
void setupWifi();
bool connectWifi();
bool reconnectMqtt();
void onReceivedMQTTMessage(char* topic, byte* payload, unsigned int payloadLength);
void processArmResponse(const char *response_str);

void setup()
{
    // Initilize baud-rate of Serial interface
    Serial.begin(115200);
    // Setup serial for ARM communication
    arm_serial.begin(ARM_BAUDRATE, SERIAL_8N1, UART1_RX, UART1_TX);
    // Wifi
    WiFi.mode(WIFI_STA);
    // MQTT
    client.setServer(MQTT_HOST, 1883);
    client.setCallback(onReceivedMQTTMessage);
    // LED
    pinMode(LED_WIFI_STATUS, OUTPUT);
    pinMode(LED_NORMAL_STATUS, OUTPUT);
    pinMode(LED_MQTT_STATUS, OUTPUT);
}


bool publishDeviceStatuses(const char *topic, JsonObject *root)
{
    const size_t json_len = measureJson(*root);
    char payload[json_len];
    bool success = false;
    serializeJson(*root, payload, json_len);
    success = client.publish(topic, payload, false);
    ESP_LOGD(FARM, "Pubished (%s) to %s:\n%s.",
             success ? "success" : "failed", topic, payload);
    return success;
}

/**
 * tokens is a pointer to a array of jsmntok_t struct
 **/
uint16_t publishDeviceStatusesInBatch(jsmntok_t *tokens[], int num_tokens, const char *original_string)
{
    uint8_t count = 0;
    uint16_t total = 0;
    String name = "";
    String status = "";
    uint16_t i = 0;
    const uint8_t batch_size = (MQTT_MAX_PACKET_SIZE - 7 - strlen(MQTT_TOPIC_ACTUATOR_STATUS)) / 7;
    StaticJsonDocument<MQTT_MAX_PACKET_SIZE * 3> doc;
    JsonObject root = doc.to<JsonObject>();
    while (i < num_tokens) {
        jsmntok_t *tok = &(*tokens)[i];  // This is the i-th key of root object
        if (tok->type != JSMN_STRING && tok->type != JSMN_PRIMITIVE) {
            i++;
            continue;
        }
        if (i % 2 == 0 && tok->type == JSMN_STRING) {
            name = "";
            size_t name_length = tok->end - tok->start;
            for (uint8_t j = 0; j < name_length; j++) {
                name += *(original_string + tok->start + j);
            }
        }
        if (i % 2 != 0 && tok->type == JSMN_PRIMITIVE && name.length()) {
            status = "";
            size_t status_length = tok->end - tok->start;
            for (uint8_t j = 0; j < status_length; j++) {
                status += *(original_string + tok->start + j);
            }
            int nstatus = strtol(status.c_str(), NULL, 10);
            root[name] = nstatus;
            count++;
            total++;
        }
        if (count && count % batch_size == 0) {
            publishDeviceStatuses(MQTT_TOPIC_ACTUATOR_STATUS, &root);
            doc.clear();
            root = doc.to<JsonObject>();
            count = 0;
        }
        i++;
    }
    if (count) {  // Still some device statuses left
        publishDeviceStatuses(MQTT_TOPIC_ACTUATOR_STATUS, &root);
    }
    return total;
}

bool publishAllDeviceStatuses(char *payload)
{
    bool success = false;
    // Extract JSON
    const size_t pl_len = strlen(payload);
    int num_tokens = 0;
    int r = 0;
    // Verfy that it is a JSON string.
    // We pass NULL to `tokens` parameters because we don't know how many tokens
    ESP_LOGD(FARM, "Payload (len=%d): %s", pl_len, payload);
    jsmn_init(&jsParser);
    num_tokens = jsmn_parse(&jsParser, payload, pl_len, NULL, 1);
    if (num_tokens == JSMN_ERROR_INVAL) {
        ESP_LOGE(FARM, "Invalid JSON string from ARM.");
        return false;
    }
    if (num_tokens == JSMN_ERROR_NOMEM) {
        ESP_LOGE(FARM, "JSON string is too big to parse.");
        return false;
    }
    if (num_tokens == 0 || num_tokens == JSMN_ERROR_PART) {
        ESP_LOGE(FARM, "Hhmm. No data. %d", num_tokens);
        return false;
    }
    if (num_tokens < 0) {
        ESP_LOGE(FARM, "Some error when parsing JSON (code: %d).", num_tokens);
        return false;
    }
    // The JSON string seems to be OK now
    // If data is small enough, send out now
    if (5 + 2 + strlen(MQTT_TOPIC_ACTUATOR_STATUS) + pl_len <= MQTT_MAX_PACKET_SIZE) {
        success = client.publish(MQTT_TOPIC_ACTUATOR_STATUS, (const uint8_t *)payload, pl_len, false);
        ESP_LOGD(FARM, "Pubished (%s) to %s:\n%s.",
                success ? "success" : "failed", MQTT_TOPIC_ACTUATOR_STATUS, (const uint8_t *)payload);
        return success;
    }
    // If the MQTT message is too big, we split the data and make smaller MQTT messages
    jsmntok_t tokens[num_tokens];
    // Parse again to get full list of tokens
    jsmn_init(&jsParser);
    r = jsmn_parse(&jsParser, payload, pl_len, tokens, num_tokens);
    if (r <= 0) {
        ESP_LOGE(FARM, "Reparsing failed (%d)!", r);
        return false;
    }
    num_tokens = r;
    // Build array of pointers to tokens, excluding the first
    jsmntok_t *pDeviceTokens[num_tokens - 1];
    ESP_LOGD(FARM, "Build array of pointer to tokens");
    for (int i = 1; i < num_tokens; i++) {
        pDeviceTokens[i - 1] = &(tokens[i]);
    }
    uint16_t total = publishDeviceStatusesInBatch(pDeviceTokens, num_tokens - 1, payload);
    return (bool) total;
}

bool publishEnvironment(char *arm_data)
{
    jsmn_parser parser;
    // The JSON if of form {"PH": 1, "EC": 1, "ST": 1}, so there are 7 tokens.
    const uint8_t num_tokens = 7;
    jsmntok_t tokens[num_tokens];
    env_cond_t cond = {0, 0, 0};
    int r = 0;
    StaticJsonDocument<JSON_OBJECT_SIZE(num_tokens)> doc;
    jsmn_init(&parser);
    r = jsmn_parse(&parser, arm_data, strlen(arm_data), tokens, num_tokens);
    if (r < 0) {
        ESP_LOGE(FARM, "ARM response data seems not to be JSON. Err %d.", r);
        return false;
    }
    /* The JSON from ARM will be {"PH": 6, "EC": 400, "ST": 20}, we will convert
     * to {"solPH": 6, "solEC": 400, "solT": 20} */
    r = parse_environment_data_string(arm_data, &cond);
    if (r < 0) {
        ESP_LOGE(FARM, "Data in ARM response looks not correct.");
        return false;
    }
    if (cond.ph) doc["solPH"] = cond.ph;
    if (SENSOR_EC_PPM) {
        if (cond.ec) doc["solEC"] = cond.ec *2;             //ppm(500) --> uS/Cm
    } else {
        if (cond.ec) doc["solEC"] = cond.ec;
    }
    if (cond.st) doc["solT"] = cond.st;
    // Prepare buffer to hold serialized data
    const size_t json_len = measureJson(doc) + 1;
    char payload[json_len];
    serializeJson(doc, payload, json_len);
    ESP_LOGI(FARM, "To publish environment data %s", payload);
    return client.publish(MQTT_TOPIC_NODE, (const uint8_t *)payload, json_len - 1, false);
}


void loop()
{
    // Connect to Wifi if connection is broken
    if (!WiFi.isConnected()) {
        connectWifi();
    }
    // Connect to MQTT if Wifi connection is established
    if (!client.connected()) {
        if (!reconnectMqtt()) {
            return;
        }
    }
    // LED --> green
    digitalWrite(LED_WIFI_STATUS, HIGH);
    digitalWrite(LED_MQTT_STATUS, HIGH);
    digitalWrite(LED_NORMAL_STATUS, LOW);
    client.loop();
    unsigned long now = millis();
    if (now >= lastDeviceStatusCheck + INTERVAL_DEVICE_CHECK) {
        lastDeviceStatusCheck = now;
        ESP_LOGD(FARM, "Send to ARM: %s", CMD_GET_ALL_ACT);
        arm_serial.write(CMD_GET_ALL_ACT "\n");
        arm_serial.flush();
        delay(10);
    }
    if (now >= lastEnvironmentCheck + INTERVAL_ENVIRONMENT_CHECK) {
        lastEnvironmentCheck = now;
        ESP_LOGD(FARM, "Send to ARM: %s", CMD_GET_ALL_ENV);
        arm_serial.write(CMD_GET_ALL_ENV "\n");
        arm_serial.flush();
        delay(10);
    }
    if (arm_serial.available()) {
        String response = arm_serial.readStringUntil('\n');
        response.trim();
        ESP_LOGD(FARM, "Got ARM response: %s", response.c_str());
        processArmResponse((const char*) response.c_str());
        // LED --> while
        digitalWrite(LED_WIFI_STATUS, LOW);
        digitalWrite(LED_NORMAL_STATUS, LOW);
        digitalWrite(LED_MQTT_STATUS, LOW);
    }
    for(int j = 0; j < 10; j++) {
        client.loop();
        delay(50);
    }
}


/* Definition of functions declared above */
bool connectWifi()
{
    unsigned int n = 0;
    uint8_t i, j = 0;
    char *ssid = NULL;
    char *password = NULL;
    bool connected = false;

    while (!connected) {
        // LED --> red
        digitalWrite(LED_MQTT_STATUS, HIGH);
        digitalWrite(LED_NORMAL_STATUS, HIGH);
        digitalWrite(LED_WIFI_STATUS, LOW);
        // Generate repeated sequence 0, 1, 2, 3, 0, 1, 2, 3...
        i = n % (sizeof(wifiLogins) / MAX_LENGTH_WIFI_STRING / 2);
        j = i << 1;  // double
        ssid = (char *)wifiLogins[j];
        password = (char *)wifiLogins[j + 1];
        ESP_LOGI(FARM, "");
        ESP_LOGI(FARM, "Connecting to %s", ssid);

        WiFi.begin(ssid, password);
        // Wait for joining Wifi
        byte c = WIFI_CONNECT_TIMEOUT;
        while (!connected && c) {
            delay(500);
            ESP_LOGI(FARM, ".");
            --c;
            connected = (WiFi.status() == WL_CONNECTED);
        }
        ++n;
    }

    ESP_LOGI(FARM, "");
    ESP_LOGI(FARM, "WiFi connected.");
    ESP_LOGI(FARM, "IP address: %s", WiFi.localIP().toString().c_str());
    return connected;
}


// ----------------------------------- CONNECT-TO-MQTT BROKER() -------------------------------//
bool reconnectMqtt()
{
    // LED --> blue
    digitalWrite(LED_MQTT_STATUS, LOW);
    digitalWrite(LED_WIFI_STATUS, HIGH);
    digitalWrite(LED_NORMAL_STATUS, HIGH);
    bool connectedMqtt = client.connect(MQTT_CLIENT_ID, MQTT_USER, MQTT_PASS);
    ESP_LOGI(FARM, "Trying to connect to " MQTT_HOST " as " MQTT_CLIENT_ID "...");
    // Attempt to connect
    if (connectedMqtt) {
        ESP_LOGI(FARM, "connected.");
        // Subscribe to the topic "baucan/d/#"
        const char topic[] = MQTT_TOPIC_ACTUATOR_CONTROL "#";
        client.subscribe(topic);
        ESP_LOGI(FARM, "Subscribed to %s.", topic);
    } else {
        ESP_LOGE(FARM, "failed, rc=%d.", client.state());
    }
    return connectedMqtt;
}

long unsigned findIDFromTopicControl(char *topic)
{
    int start_search = strlen(MQTT_TOPIC_ACTUATOR_CONTROL);
    char *substr = topic + start_search;
    return strtoul(substr, NULL, 10);
}


/**
 * Callback function to be called when a message is received from MQTT.
 * Ref: https://pubsubclient.knolleary.net/api.html#callback
 **/
void onReceivedMQTTMessage(char* topic, uint8_t* payload, unsigned int payloadLength)
{
    if (payloadLength > MQTT_MAX_EXPECTED_PAYLOAD_LENGTH) {
        ESP_LOGD(FARM, "Received MQTT payload is too big (%d). Skip.", payloadLength);
        return;
    }
    long unsigned int device_id = 0;
    char command[MAX_CMD_LEN] = {0};
    uint8_t desiredState = 0;
    /**
     * Make String object out of received payload.
     * Note that `payload` pointer points to the same buffer as out-going message,
     * so it also contains rubbish data. We have to exclude them when making String.
     **/
    String payloadStr = String("");
    for (unsigned int i = 0; i < payloadLength; i++) {
        payloadStr.concat(char(*(payload + i)));
    }
    // Remove all whitespaces
    payloadStr.trim();
    // Get desired state of the devices
    desiredState = payloadStr.toInt();
    ESP_LOGD(FARM, "Topic %s", topic);
    // Find device ID
    device_id = findIDFromTopicControl(topic);
    sprintf(command, CMD_SET_STATE, device_id, desiredState);
    ESP_LOGD(FARM, "Send to ARM: %s", command);
    arm_serial.write(command); arm_serial.write("\n");
    // We just write to serial connection and don't need to wait for response here.
    // The response will be checked in loop() function.
}

void processArmResponse(const char *response_str)
{
    arm_resp_t rsp;
    armrp_init(&rsp);
    bool valid = parse_arm_response(response_str, &rsp);
    if (!valid) {
        ESP_LOGD(FARM, "%s", response_str);
        ESP_LOGE(FARM, "Not a valid ARM response");
    }
    if (!rsp.is_ok) {
        ESP_LOGE(FARM, "ARM module got some error when doing its task");
    }
    /* FIXME: Not sure about lifetime of rsp.data buffer. */
    switch (rsp.cmd)
    {
        case ARM_AA:
        case ARM_GA:
        case ARM_SA:
            publishAllDeviceStatuses(rsp.data);
            break;

        case ARM_AE:
            publishEnvironment(rsp.data);
            break;

        default:
            break;
    }
}
